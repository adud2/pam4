#! /usr/bin/env python3

import pickle
from argparse import ArgumentParser, Action
import gencarto as gc
from os.path import join
import inspect
from utils import SimRes


def compute_cartos(pitchs, simf, **kwargs):
    """compute many cartographies at given pitches
    pitchs: the list of the pitchs to simulate
    simf: the target folder to put the simulations
    kwargs are simply passed to pitch_gamma"""
    for p in pitchs:
        print("pitch: %3d")
        sim = gc.pitch_gamma(p, **kwargs)
        with open(join(simf, "%s.bin" % str(p)), "wb") as of:
            pickle.dump(sim, of)


def draw_cartos(pitchs, simf, mapf, fnote):
    """draw many cartographies of pitches given simulation files
    and name them from 0 to <total number of maps>
    pitchs: the list of pitches
    mapf: the folder to put the maps
    fnote: the pitch of the lowest simulation
    (can be useful if one wishes to redraw only part of the maps)"""
    def fname(sim, gr):
        p = sim.args['p']
        n_gr = sim.args['n_gr']
        return str((p - fnote) * n_gr + gr)

    for p in pitchs:
        with open(join(simf, "%s.bin" % str(p)), "rb") as rf:
            sim = pickle.load(rf)
            gc.svm_de_tranches(sim, mapf, fname)


class StoreDictKeyPair(Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        self._nargs = nargs
        super(StoreDictKeyPair, self).__init__(
            option_strings, dest, nargs=nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        my_dict = {}
        for kv in values:
            k, v = kv.split("=", 1)
            my_dict[k] = int(v) if v.isdigit() else float(v)
        setattr(namespace, self.dest, my_dict)


parser = ArgumentParser()
parser.add_argument("simf", type=str, help="the simulation folder")
parser.add_argument("first", type=int, help="the first note (MIDI)")
parser.add_argument("last", type=int, help="the last note (MIDI) (excl)")
parser.add_argument("--minp", type=int, default=None,
                    help="the lowest note in all simulations (MIDI)")
parser.add_argument("--mapf",
                    help="draw maps from simulations in the map folder")
parser.add_argument("--nc", action="store_true",
                    help="don't compute the simulation")
parser.add_argument("--opts", action=StoreDictKeyPair, nargs="+",
                    metavar="KEY=VAL", default=dict(),
                    help="options of the simulation"
                    "(see frequency_gamma function in gencarto.py)")

if __name__ == '__main__':
    c = parser.parse_args()
    pitchs = range(c.first, c.last)
    if c.minp is None:
        c.minp = c.first
    if not c.nc:
        compute_cartos(pitchs, c.simf, **c.opts)
    if c.mapf:
        draw_cartos(pitchs, c.simf, c.mapf, c.minp)
