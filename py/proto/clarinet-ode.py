"""implementing clarinet model in Missoum et al"""

import numpy as np
from matplotlib import use as muse
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy.io.wavfile import write


muse('Qt5Agg')


kg = 1  # one kilogram
m = 1  # one meter
s = 1  # one second
Hz = int(1/s)  # one Hertz
mm = 1e-3 * m  # one millimeter

LEN = .4 * m  # length of the tube
C = 340 * m / s  # speed of sound
ABS = 1.044  # related to absorbtion coeff
IRAD = 7 * mm  # interior radius
VISC = 1.708e-5 * kg / m / s  # viscosity
DENS = 1.29 * kg / m ** 3  # density of the air

N = 10  # truncature of the modal decomposition


SR = 22050 * Hz  # Sampling rate


def angFreq(n):
    "angular frequency of the n-th mode"
    return (2 * n - 1) * np.pi * C / 2 / LEN


# Damping 1 / 2Q
def dampC(n):
    "compute the damping coefficient of the n-th mode"
    gn = (2 * n - 1) * np.pi / 2
    return ABS / IRAD * np.sqrt(VISC * LEN / DENS / C / gn) \
        + 1/2 * gn * (IRAD / LEN)**2


def coeffsMod(n):
    """compute the constant coefficients of each pressure mode
    n: the number of modes

    Returns: 2 arrays of size n for resp. dp_i/dt coeff and p_i coeff"""
    out = np.zeros((2, n))
    angFreqV = np.array([angFreq(i) for i in range(1, n+1)])
    dampCV = np.array([dampC(i) for i in range(1, n+1)])
    out[0] = angFreqV ** 2
    out[1] = (2 * angFreqV * dampCV)
    return out


def oSqrt(x):
    """odd extension of the square root"""
    return np.sqrt(np.abs(x)) * np.sign(x)


def flow(p, blow, zeta):
    """compute the air flow through the mouthpiece
    p: normalized pressure inside the mouthpiece
    blow: control parameter (normalized blowing pressure)
    zeta: control parameter (embouchure)"""
    diffp = blow - p  # difference of pressure
    if diffp <= 1:
        return zeta * (1 - diffp) * oSqrt(diffp)
    return 0


def dFlow(p, blow, zeta, dp, dBlow, dZeta):
    """compute the derivative of the function above
    p: normalized pressure inside the mouthpiece
    blow: control parameter (normalized blowing pressure)
    zeta: control parameter (embouchure)
    dp: derivative of p
    dBlow: derivative of blow
    dZeta: derivative of zeta"""
    diffp = blow - p
    dDiffp = dBlow - dp
    if diffp <= 1:
        return dZeta * (1 - diffp) * oSqrt(diffp) \
            - zeta * dDiffp * oSqrt(diffp) \
            + zeta * (1 - diffp) * 1 / 2 / np.sqrt(np.abs(diffp))
    return 0

# consider Y point of the phase space of dimension (2 * N) such that
# Y[i] : pressure of ith mode
# Y[N+i] : differential of the pressure of the ith mode


def dYgen(t, Y,
          dYOut,
          coeffsM,
          blowF, zetaF, dBlowF, dZetaF):
    """a generic differential of the system Y for a clarinet
    ODE Params
    ---------
    t: time
    Y: point in phase space
    dYout: array that will contain the result at the end

    Precomputed params
    ------------------
    coeffsM: an (2, N) array of the constant coefficients of each mode
             first line: coeffs of p
             second line: coeffs of dp/dt

    Control params
    --------------
    blowF: blow as function of time
    zetaF: zeta as function of time
    dBlowF: differential of blow as function of time
    dZetaF: differential of zeta as function of time
    """
    n = Y.shape[0] // 2
    p = np.sum(Y[:n])
    dp = np.sum(Y[n:])

    dYOut[:n] = Y[n:]
    dYOut[n:] = 2 * C / LEN \
        * dFlow(p, blowF(t), zetaF(t), dp, dBlowF(t), dZetaF(t))
    dYOut[n:] -= coeffsM[0] * Y[:n] + coeffsM[1] * Y[n:]
    return dYOut


if __name__ == '__main__':

    def cstBlow(t):
        return 0.5

    def dCstBlow(t):
        return 0

    def cstZeta(t):
        return 0.5

    def dCstZeta(t):
        return 0

    dYout = np.zeros(2 * N)
    coeffsM = coeffsMod(N)

    def dY(t, Y):
        return dYgen(t, Y, dYout, coeffsM,
                     cstBlow, cstZeta, dCstBlow, dCstZeta)

    init = np.zeros(2*N)

    dur = 2 * s
    sol = solve_ivp(dY, (0, dur), init, method='RK23',
                    t_eval=np.linspace(0, dur, dur * SR, endpoint=False))

    press = np.sum(sol.y[:N], axis=0)

    npress = np.int32(press / np.max(np.abs(press)) * (1 << 30))
    write('tamaman.wav', SR, npress)

    plt.clf()
    plt.plot(sol.t, npress / (1 << 30))
    plt.plot(sol.t[::1000], [cstBlow(t) for t in sol.t[::1000]])
    plt.show()
