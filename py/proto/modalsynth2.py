import numpy as np
import scipy.signal as sci


class ModalSynthesisClarinet():
    """General class used to simulate the dynamic behavior of the clarinet
    using a modal decomposition of the bore """

    def __init__(self, L, sr=44100):
        """Initialization of the class, pre-computes all constants

        Args:
            L (float): length of the bore in meters
            sr (int, optional): sampling rate in Hertz. Defaults to 44100.
        """
        # Units
        self.kg = 1  # one kilogram
        self.m = 1  # one meter
        self.s = 1  # one second
        self.Hz = int(1/self.s)  # one Hertz
        self.mm = 1e-3 * self.m  # one millimeter
        self.Pa = 1  # one Pascal

        # General simulation parameters
        self.LEN = L * self.m  # length of the tube
        self.C = 340 * self.m / self.s  # speed of sound
        self.sr = sr
        self.Te = 1 / self.sr
        self.ABS = 1.044  # related to absorbtion coeff
        self.IRAD = 7 * self.mm  # interior radius
        self.SECTION = np.pi*self.IRAD**2  # Surface section
        self.VISC = 1.708e-5 * self.kg / self.m / self.s  # viscosity
        self.DENS = 1.29 * self.kg / self.m ** 3  # density of the air
        self.ZC = self.C*self.DENS/self.SECTION

        # Reed oscillation parameters
        self.Kr = 8.66*10**6 * self.Pa / self.m  # Reed stiffness
        self.Mr = 0.05 * self.kg / self.m**2  # Reed mass
        self.Gr = 3000 / self.s  # Damping coefficient
        self.YMr = 4*10**(-4)*self.m  # Opening at the tip of the reed at rest
        self.WIDTHr = 0.013 * self.m  # Width of the reed
        self.Pclosed = self.Kr * self.YMr * self.Pa  # Closing pressure
        self.Wr = np.sqrt(self.Kr / self.Mr)  # Omega reed
        self.Wr_2 = self.Wr**2  # Omega squared reed

        # Coefficients for regularization function
        self.Knlr = 1/0.4
        self.C_nlr = 0.4*np.exp(1.5)

        # Reed parameters with corresponding mutlitplication by Te for use in
        # the discretized equation
        self.Wr_2_Te = self.Wr_2 * self.Te**2
        self.Gr_Te = self.Gr * self.Te

        # Waveguide oscillations parameters
        # Modal truncature to ensure stability
        self.N_mode = int((self.sr / 6 * 4 * self.LEN/self.C - 1) / 2 + 1)
        # Modals pulsations of the waveguide
        self.ws = (2*np.arange(self.N_mode)+1) * 2 * np.pi\
            * self.C / (4*self.LEN)
        self.ws_2 = self.ws**2  # Squared pulsations

        # Quality factors of resonances
        self.Qs = 1 / self.dampC(np.arange(self.N_mode)+1)

        # Waveguide parameters for use in the discretized equation
        self.c2l = 2 * self.C * self.Te / self.LEN
        self.ws_2_Te = self.ws_2 * self.Te**2
        self.ws_Q_Te = self.ws * self.Te / self.Qs

        # U coeff
        # Constant coefficient use for computation of the volume flow
        self.Ucoeff = self.ZC * self.WIDTHr * self.YMr \
            * np.sqrt(2 / self.DENS / self.Pclosed)

    def dampC(self, n):
        "compute the damping coefficient of the n-th mode"
        gn = (2 * n - 1) * np.pi / 2
        return self.ABS / self.IRAD\
            * np.sqrt(self.VISC * self.LEN / self.DENS / self.C / gn) \
            + 1/2 * gn * (self.IRAD / self.LEN)**2

    def compute_u(self, y, pm, p):
        """Returns the volume flow u through the mouthpiece
        given the reed displacement, pressure in the mouth
        and pressure in the mouthpiece

        Args:
            y (float): redd displacement
            pm (float): pressure in the mouth
            p (float): pressure in the mouthpiece

        Returns:
            float: computed volume flow
        """
        return self.Ucoeff * (1 - y)*np.sign(pm-p)*np.sqrt(np.abs(pm-p))

    def simulate(self, duration, gamma, zeta, gr):
        """Simulates the behavior of the clarinet using given parameters
        using 0 as initial condition for all parameters and acoustic values.

        Args:
            duration (float): duration of the simulation
            gamma (function): adimensionned pressure in the player's mouth.
                The function must take an array as input (time vector) and
                return an array of same size

            zeta (function): adimensionned force applied by the player's mouth
                             on the reed
                The function must take an array as input (time vector) and
                return an array of same size

            gr (function): adimensionned damping coefficient of the reed
                           (0 gives 3000/s, 1 gives 5000/s)
                The function must take an array as input (time vector) and
                return an array of same size

        Returns:
            p (array): simulated pressure at the entrance of the bore
            p (array): simulated volume flow at the entrance of the bore
            p (array): simulated reed displacement
            p (array): time vector
        """

        t = np.linspace(0, duration, int(self.sr*duration))

        p = np.zeros((len(t)+2))
        pis = np.zeros((self.N_mode, len(p)))
        u = np.zeros_like(p)
        yr = np.zeros_like(u)
        yr_nl = np.zeros_like(u)

        # control parameters adimensionned
        gammadis = gamma(t)
        zetadis = zeta(t)
        Grdis = gr(t)*2000 + 3000

        gammadis = np.concatenate((np.zeros(2), gammadis))
        zetadis = np.concatenate((np.zeros(2), zetadis))
        Grdis = np.concatenate((np.zeros(2), Grdis))

        steps = np.arange(2, len(p))

        for step in steps:
            for i, pi in enumerate(pis):
                # Resolve ODE on p at current time step for each mode
                pi[step] = pi[step-1]\
                    * (2 - self.ws_Q_Te[i] - self.ws_2_Te[i])\
                    + pi[step-2] * (self.ws_Q_Te[i] - 1)\
                    + self.c2l * (u[step-1] - u[step-2])

            p[step] = np.sum(pis[:, step])  # Sum of contributions of all modes

            # Compute displacement of the reed tip at current time step
            yr[step] = yr[step-1] * (2 - Grdis[step-1]*self.Te - self.Wr_2_Te)\
                + yr[step-2] * (self.Te * Grdis[step-1] - 1)\
                + self.Wr_2_Te * (gammadis[step-1] - p[step-1] +
                                  zetadis[step-1])

            yr_nl[step] = self.compute_yr_nl(yr[step])

            # Compute u at current time step using pressure drop value and reed
            # opening
            u[step] = self.compute_u(yr_nl[step], gammadis[step], p[step])

        return p[2:], u[2:], yr_nl[2:], t

    def compute_yr_nl(self, y):
        """Returns a modified value of the redd tip displacement to ensure that
        it does not go through the mouthpiece. The function is chose in order
        to try to emulate a non-linear stiffness due to partial contact of the
        reed with the mouthpiece when the reed diplacement goes over 0.6.

        Args:
            y (array): reed tip displacement

        Returns:
            out (array): reed tip displacement after regularization

        """
        if y < 0.6:
            return y
        else:
            return 1-self.C_nlr*np.exp(-self.Knlr * y)

    def apply_filter(self, q):
        """Apply a filter to the pressure in the moutpiece to obtain the
        radiated pressure

        Args:
            q (array): signal to filter

        Outputs:
            out (array): filtered signal

        """
        BW = 0.12
        fc = 1800
        K = 4

        k1 = -np.cos(2*np.pi*fc/self.sr)
        k2 = (1-np.tan(BW/2))/(1+np.tan(BW/2))

        Pz = np.array([1, k1*(1+k2), k2])  # define denominator coefficients
        Qz = np.array([k2, k1*(1+k2), 1])  # define numerator coefficients
        Num = (Pz*(1+K) + Qz*(1-K))/2
        Den = Pz

        out = sci.lfilter(Num, Den, q)

        return out
