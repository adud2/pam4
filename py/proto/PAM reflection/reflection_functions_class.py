import numpy as np

class ReflectionFunction:
    """General Class for unique Reflection Function

    Returns: 
        array: [self.reflection_function, self.min_time, self.max_time]
    """
    def __init__(self, d, c=340):
        """Constructor of the reflection class such that the reflection function is 
        a dirac in t=T with T = d / c

        Args:
            d (float): round_trip length in meters
            c (float): wave speed
        """
        self.d = d
        self.c = c
        self.T = self.d/self.c
        
    def reflection_function(self):
        """Return an array with the reflection function and the minimum time
        and maximum time between which it'll be estimated

        Args:
            t (float): delay time
        """
        def r_func(t):
            if t!=self.T:
                return 0
            else:
                return 1
        return [r_func, self.T, self.T]
   
    
class RefFuncGauss(ReflectionFunction):
    """Returns an array for the reflection function as a gaussian curve

    Args:
        ReflectionFunction (class): general class for single reflection functions

    Returns:
        array: array with the reflection_function and the interval of interest
    """
    def __init__(self, d, c=340, width_factor = 20/100, epsilon = 10**(-4)):
        """Constructor of the reflection class such that the reflection function is 
        a gaussian curve in t=T with T = d / c

        Args:
            d ([type]): round trip length
            c (int, optional): wave speed. Defaults to 340.
            width_factor ([type], optional): width of the gaussian curve. Defaults to 20/100.
            epsilon ([type], optional): mnimum value for which the reflection curve is not set to 0. Defaults to 10**(-4).
        """
        self.d = d
        self.c = c
        self.width_factor = width_factor
        self.T = self.d/self.c
        self.b = 4*np.log(2) / (self.width_factor*self.T)**2
        self.min_time = self.T - np.sqrt(np.log(epsilon)/(-self.b))
        self.max_time = self.T + np.sqrt(np.log(epsilon)/(-self.b))
        
    def reflection_function(self):
        """Return an array with the reflection function and the minimum time
        and maximum time between which it'll be estimated

        Args:
            t (float): delay time
        """
        def r_func(t):
            return -np.exp(-self.b*(t-self.T)**2) * (t>=0)
        return [r_func, self.min_time, self.max_time]

    
class ReflectionFunction2:
    """General Class for two Reflection Function
    """
    def __init__(self, d, c=340):
        """Constructor of the reflection class such that the reflections functions are 
        diracs in t=T with T = d / c

        Args:
            d (array): array of round_trips lengths [dleft, dright]
            c (float): wave speed
        """
        self.d = d
        self.c = c
        self.Tl = self.d[0]/self.c
        self.Tr = self.d[1]/self.c
        self.T = min(self.Tl,self.Tr)
        
    def reflection_function(self):
        """Return an array of arrays with the reflection function and the minimum time
        and maximum time between which it'll be estimated, left first, right second.

        Args:
            t (float): delay time
        """
        def r_funcl(t):
            if t!=self.Tl:
                return 0
            else:
                return 1
        def r_funcr(t):
            if t!=self.Tr:
                return 0
            else:
                return 1        
        return [[r_funcl, self.Tl, self.Tl], [r_funcr, self.Tr, self.Tr]]


class RefFuncGauss2(ReflectionFunction2):
    """Returns an array for the double reflection function as a gaussian curve

    Args:
        ReflectionFunction (class): general class for double reflection functions
    """
    def __init__(self, d, c=340, width_factor = np.array((20/100,20/100)), epsilon = 10**(-4)):
        """Constructor of the reflection class such that the reflections functions are 
        gaussian curves in t=T with T = d / c

        Args:
            d (array): array of round_trips lengths [dleft, dright]
            width_factor (array, optional): [description]. Defaults to 20/100.
            epsilon ([type], optional): Value below which the reflection function will not be assumed to be zero. Defaults to 10**(-4).

        Returns:
            array: array of 2 arrays with the reflection_functions and the intervals of interests
    """
        super().__init__(d, c)
        self.width_factor = width_factor
        self.epsilon = epsilon
    
    def reflection_function(self):
        l_reflection = RefFuncGauss(self.d[0], self.c, self.width_factor[0], self.epsilon).reflection_function()
        r_reflection = RefFuncGauss(self.d[1], self.c, self.width_factor[1], self.epsilon).reflection_function()
        return [l_reflection, r_reflection]

