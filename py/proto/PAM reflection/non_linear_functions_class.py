import numpy as np

class NonLinearCaracteristic:
    """General class for the non linear caracteristic representing f = F(q)
    Must contain a function F and a function intersect wich is used to find the solution of the equation q-qh-ZF(q) = 0
    """
    def __init__(self, Z=1):
        self.Z = Z
        
    def F(self, q, pcontrol=0, control2=0):
        """Non linear caracteristic given for the identity function

        Args:
            q (float): value of q

        Returns:
            float: associated value of f
        """
        return q  
    
    def intersect(self, qh, pcontrol=0, control2=0, last_state=0, last_q=0):
        """Function returning the couple [q,f] of solutions to q-qh-ZF(q) = 0 corresponding with the given qh

        Args:
            qh ([type]): value of qh
            pcontrol (int, optional): Useless in this case.
            control2 (int, optional): Useless in this case.
            last_state (int, optional): Useless in this case.
            last_q (int, optional): Useless in this case.

        Returns:
            [type]: [description]
        """
        if self.Z != 1:
            q = qh/(1-self.Z)
            Fq = self.F(q)
            return [q, Fq, last_state]
        else:
            return [0, 0, last_state]
   
    
class NonLinearCaracteristicSingleReed(NonLinearCaracteristic):
    """General class for the non linear caracteristic representing f = F(q) for single reed insturments.
    Must contain a function F and a function intersect wich is used to find the solution of the equation q-qh-ZF(q) = 0
    This class has an attribute self.qc representing the closing pressure of the reed that the general NonLinearCaracteristic class 
    does not have.
    """
    def __init__(self, Z=1, qc=-2):
        super().__init__(Z)
        self.qc = qc
        
    def F(self, q, pcontrol=0, control2=0):
        if q>self.qc:
            return super().F(q, pcontrol, control2)
        else:
            return 0
    def intersect(self, qh, pcontrol=0, control2=0, last_state=0, last_q=0):
        if (qh + self.qc > 0):
            return [-qh, 0, last_state]
        else:
            return super().intersect(qh, pcontrol, control2, last_state, last_q)
        
        
class FSquaredSingleReed(NonLinearCaracteristicSingleReed):
    """Class for the non linear caracteristic of single reed instruments expressed as a 2nd degree 
    polynomial function in q : f = k*(p-q)*(q-qc)

    Args:
        NonLinearCaracteristicSingleReed (class): General container class for single reed nonlinear caracteristic
    """
    def __init__(self, Z=1, qc=-2, espilon = 10**(-4)):
        super().__init__(Z, qc)
        self.epsilon = espilon
        
    def F(self, q, pcontrol, k):
        """This function returns the value of 
        f = k*(p-q)*(q-qc)

        Args:
            q (float): value of the pressure at the entrance of the bore
            pcontrol (float): value of the pressure in the mouth
            k (float): value of the amplitude parameter of the non linear caracteristic

        Returns:
            f (float): value of the volume flow at the entrance of the bore
        """
        if q<self.qc:
            return 0 #Reed closed
        else:
            return k*(pcontrol-q)*(q-self.qc) #* (q+p-2*self.qc)
        
    def intersect(self, qh, pcontrol, k, last_state, last_q):
        """This function resolves q-q_h-Zf for q with 
        F(q) = 0 if q<qc, F(q) = k*(p-q)*(q-qc) otherwise

        Args:
            qh (float): current value of qh such that qh(t) = r(t) * (q(t)+Zf(t))
            pcontrol (float): current pressure in the mouthpiece
            last_state (bool): Useless in this case, added for coherence
            k (float): value of the amplitude parameter of the non linear caracteristic

        Returns:
            [float, float, bool]: an array containing q,h and state with q the solution to the equation and f the value of F(q) associated.
                                In this case, state is useless but is added for coherence
        """
        # we have to solve q^2 + q(1/(Z*k) -(qc+pcontrol)) + (qc*pcontrol-qh/(Z*k)) = 0
        #if (last_q-qh-self.Z*self.F(last_q, pcontrol,k)) < self.epsilon:
        #    return [last_q, self.F(last_q, pcontrol, k), last_state]
        
        if qh < self.qc:
            return [qh, 0, last_state]
        else:
            a = 1
            b = 1/(self.Z*k) - (pcontrol+self.qc)
            c = -qh/(self.Z*k) + self.qc*pcontrol
            delta = b**2-4*a*c
            q = (-b+np.sqrt(delta))/(2*a)
            return [q, self.F(q, pcontrol, k), last_state]


class FCubeSingleReed(NonLinearCaracteristicSingleReed):
    """Class for the non linear caracteristic of single reed instruments expressed as a 2nd degree 
    polynomial function in q : f = k*(p-q)*(q-qc)*(q+p-2*qc)

    Args:
        NonLinearCaracteristicSingleReed (class): General container class for single reed nonlinear caracteristic
    """
    def __init__(self, Z=1, qc=-2, epsilon = 10**(-4)):
        super().__init__(Z, qc)
        self.epsilon = epsilon
        
    def F(self, q, pcontrol, k):
        return (k*(pcontrol-q)*(q-self.qc)*(q+pcontrol-2*self.qc))*(q>self.qc)
    
    def intersect(self, qh, pcontrol, k, last_state, last_q):
        """This function resolves q-q_h-Zf for q with 
        F(q) = 0 if q<qc, F(q) = k*(p-q)*(q-qc)*(q+p-2*qc) otherwise

        Args:
            qh (float): current value of qh such that qh(t) = r(t) * (q(t)+Zf(t))
            pcontrol (float): current pressure in the mouthpiece
            last_state (bool): Useless in this case, added for coherence
            k (float): value of the amplitude parameter of the non linear caracteristic
            last_q (array): last value of q
            
        Returns:
            [float, float, bool]: an array containing q,h and state with q the solution to the equation and f the value of F(q) associated.
                                In this case, state is useless but is added for coherence
        """
        if qh<self.qc:
            return [qh, 0, last_state]
        def test_func(q):
            return q-qh-self.Z*self.F(q, pcontrol, k)
        epsilonlast = test_func(last_q)
        if np.abs(epsilonlast) < self.epsilon:
            return [last_q, self.F(last_q, pcontrol, k), last_state]
        elif epsilonlast > self.epsilon:
            sup_mark = last_q
            inf_mark = last_q
            i=1
            while test_func(inf_mark)>0:
                sup_mark = inf_mark
                inf_mark -= 0.1*2**i
            qsol = self.dichotomy(test_func,inf_mark,sup_mark)
            return [qsol, self.F(qsol, pcontrol, k), last_state]
        else:
            inf_mark = last_q
            sup_mark = last_q
            i=1
            while test_func(sup_mark)<0:
                inf_mark = sup_mark
                sup_mark += 0.1*2**i
                i+=1
            qsol = self.dichotomy(test_func,inf_mark,sup_mark)
            return [qsol, self.F(qsol, pcontrol, k), last_state]
    
    def dichotomy(self,test_func, inf_mark, sup_mark):
        mid_mark = (sup_mark + inf_mark) /2
        mid_mark_result = test_func(mid_mark)
        if -self.epsilon<mid_mark_result<self.epsilon:
            return mid_mark
        elif mid_mark_result>self.epsilon:
            return self.dichotomy(test_func, inf_mark, mid_mark)
        elif mid_mark_result<-self.epsilon:
            return self.dichotomy(test_func, mid_mark, sup_mark)
        

class FSquareRootSingleReed(NonLinearCaracteristicSingleReed):
    """Class for the non linear caracteristic of single reed instruments expressed as a 2nd degree 
    polynomial function in q : f = k*(p-q)*(q-qc)*(q+p-2*qc)

    Args:
        NonLinearCaracteristicSingleReed (class): General container class for single reed nonlinear caracteristic
    """
    def __init__(self, Z=1, qc=-1, epsilon = 10**(-4)):
        super().__init__(Z, qc)
        self.epsilon = epsilon
        
    def F(self, q, pcontrol, k):
        qdiff = q-pcontrol
        return k * (self.qc-q)*np.sqrt(np.abs(qdiff)) * np.sign(qdiff) * ((q-self.qc)>0)
    
    def intersect(self, qh, pcontrol, k, last_state, last_q):
        """This function resolves q-q_h-Zf for q with 
        F(q) = 0 if q<qc, F(q) = k*(p-q)*(q-qc)*(q+p-2*qc) otherwise

        Args:
            qh (float): current value of qh such that qh(t) = r(t) * (q(t)+Zf(t))
            pcontrol (float): current pressure in the mouthpiece
            last_state (bool): Useless in this case, added for coherence
            k (float): value of the amplitude parameter of the non linear caracteristic
            last_q (array): last value of q
            
        Returns:
            [float, float, bool]: an array containing q,h and state with q the solution to the equation and f the value of F(q) associated.
                                In this case, state is useless but is added for coherence
        """
        if qh<self.qc:
            return [qh, 0, last_state]
        def test_func(q):
            return q-qh-self.Z*self.F(q, pcontrol, k)
        epsilonlast = test_func(last_q)
        if np.abs(epsilonlast) < self.epsilon:
            return [last_q, self.F(last_q, pcontrol, k), last_state]
        elif epsilonlast > self.epsilon:
            sup_mark = last_q
            inf_mark = last_q
            i=1
            while test_func(inf_mark)>0:
                sup_mark = inf_mark
                inf_mark -= 0.1*2**i
            qsol = self.dichotomy(test_func,inf_mark,sup_mark)
            return [qsol, self.F(qsol, pcontrol, k), last_state]
        else:
            inf_mark = last_q
            sup_mark = last_q
            i=1
            while test_func(sup_mark)<0:
                inf_mark = sup_mark
                sup_mark += 0.1*2**i
                i+=1
            qsol = self.dichotomy(test_func,inf_mark,sup_mark)
            return [qsol, self.F(qsol, pcontrol, k), last_state]
    
    def dichotomy(self,test_func, inf_mark, sup_mark):
        mid_mark = (sup_mark + inf_mark) /2
        mid_mark_result = test_func(mid_mark)
        if -self.epsilon<mid_mark_result<self.epsilon:
            return mid_mark
        elif mid_mark_result>self.epsilon:
            return self.dichotomy(test_func, inf_mark, mid_mark)
        elif mid_mark_result<-self.epsilon:
            return self.dichotomy(test_func, mid_mark, sup_mark)


class NonLinearCaracteristicBowedString(NonLinearCaracteristic):
    def __init__(self, Z=1):
        super().__init__(Z)
        
    def F(self, q, pcontrol, Q):
        """This function returns the value of 
        f = 1/(p-q+Q)

        Args:
            q (float): value of the velocity of the string at the bowing point
            pcontrol (float): velocity of the bow
            Q (float): "quality factor" of the curve

        Returns:
            float : value of f(q), force in the string at the bowing point
        """
        if q<pcontrol:
            return 1/(pcontrol-q+Q)
        else:
            return 1/(pcontrol-q-Q)
    
    def intersect(self, qh, pcontrol, Q, last_state, last_q):
        """This function resolves q-q_h-Zf for q with 
        F(q) = 1/(p-q+Q) if q<p, F(q) = 1/(p-q-Q) otherwise

        It also returns the current state to solve hysteresis next time it's called
        Args:
            qh (float): current value of qh such that qh(t) = r(t) * (q(t)+Zf(t))
            pcontrol (float): current velocity of the bow
            Q (float): "quality factor" of the curve
            last_state (bool): last state of the coupling : 1(or True) for sticking state, 
                            0 (or False) for slipping state
                            

        Returns:
            [float, float, bool]: an array containing q,h and state with q the solution to the equation and f the value of F(q) associated
        """
        # we have to solve q^2 - q(qh+pcontrol) + (pcontrol*qh+Z) = 0
        if qh<pcontrol:
            a = 1
            b = -(Q+qh+pcontrol)
            c = (pcontrol+Q)*qh+self.Z
        else:
            q = pcontrol
            f = (q-qh)/self.Z
            return[pcontrol, f, 1] 
        
        delta = b**2 - 4*a*c
        
        #If no solution, then the intersection is on the vertical
        #asymptot, so q = pcontrol :
        if delta<0:
            q = pcontrol
            f = (q-qh)/self.Z
            return[pcontrol, f, 1] 
        
        if qh<pcontrol-self.Z/Q:
            q = (-b-np.sqrt(delta))/2*a
            f = self.F(q, pcontrol, Q)
            return[q, f, 0]
        elif qh>pcontrol+self.Z/Q:
            q = (-b+np.sqrt(delta))/2*a
            f = self.F(q, pcontrol, Q)
            return[q, f, 0]
        else:
            if last_state==1:
                q = pcontrol
                f = (q-qh)/self.Z
                return[pcontrol, f, 1] 
            else:
                if qh<pcontrol:
                    q = (-b-np.sqrt(delta))/2*a
                    f = self.F(q, pcontrol, Q)
                    return[q, f, 0]
                else:
                    q = (-b+np.sqrt(delta))/2*a
                    f = self.F(q, pcontrol, Q)
                    return[q, f, 0]
                

