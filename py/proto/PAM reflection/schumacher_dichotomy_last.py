import numpy as np
import matplotlib.pyplot as plt
from time import sleep
import matplotlib.animation as animation
from IPython.display import HTML, display, Image
import reflection_functions_class as rf
import non_linear_functions_class as nl

class ModelInstrument:
    """General class for model instruments
    """
    def __init__(self, ReflectionFunction,NonLinearFunction):
        self.ReflectionFunction = ReflectionFunction
        self.NonLinearFunction = NonLinearFunction
        
        self.Z = self.NonLinearFunction.Z
        self.r = self.ReflectionFunction.reflection_function()
        self.round_trip = self.ReflectionFunction.T
        self.F = self.NonLinearFunction.F
        self.intersect = self.NonLinearFunction.intersect


class BuildSimulator:
    """General class used to buil the simulator from a non linear caracteristic function and a reflection function

    Returns:
        class: Simulator associated with given functions
    """
    def __init__(self, ReflectionFunction, NonLinearFunction, timestep = 0, plot_dir = False, loss = 0):
        self.ReflectionFunction = ReflectionFunction
        self.NonLinearFunction = NonLinearFunction
        self.model_instrument = ModelInstrument(self.ReflectionFunction, self.NonLinearFunction)
        self.loss = loss
        if timestep == 0:
            self.timestep = self.model_instrument.round_trip / 128
        else:
            self.timestep = timestep
        self.plot_dir = plot_dir
        
    def __call__(self):
        if issubclass(type(self.ReflectionFunction), rf.ReflectionFunction):
            if self.ReflectionFunction.reflection_function()[1] == self.ReflectionFunction.reflection_function()[2]:
                print("Building Osc Simulator Dirac")
                return AutoOscSimulatorDirac(self.model_instrument, self.timestep, loss = self.loss)
            else:
                print("Building Osc Simulator 1 reflection")
                return AutoOscSimulator1ref(self.model_instrument, self.timestep, loss = self.loss)
        else:
            print("Building Auto Osc Simulator 2 reflection")
            return AutoOscSimulator2ref(self.model_instrument, self.timestep, loss = self.loss)
        

class AutoOscSimulator1ref:
    """This class implement the general algorithm  to simulate auto-ouscillating instruments in time domain 
    presented by Mcintyre, Woodhouse and Schumacher in 1983
    
    Equations to solve at each time step are the following, where f is the volume flow or the force and q is the pressure or the speed : 
    f = F(q),
    q(t) = q_h(t) + Zf(t) and
    q_h(t) = r(t) * (q(t) + Zf(t))    
    
    
    Properties :
        f : non-linear function q(t)-q_h(t)-Zf(t), must be equal to zero at each step.
            takes three parameters : q(t), q_h() and the control parameter p(t).
            If the function accepts more than one solution for a given value of p_h(t),
            it takes a fourth argument giving the last state in order to solve the hysteresis rule.
            
        r : reflection function allowing to compute this ingoing wave from the outgoing wave. Of the form [r,min_value,max_value] where f is the function, 
            and [min_value,max_value] is the interval in which it is not negligible and for which the integral is computed. 
            for wind insruments, r must be expressed with p and q as arguments where p is the pressure inside the mouthpiece and q is the pressure at the entrance of the bore
            
        Z : caracteristic impedance (or admittance) of the medium.
        
        timestep : période d'échantillonage
        
    """
    def __init__(self, model_instrument, timestep, plot_dir=False, loss=0):
        self.intersect = model_instrument.intersect
        self.r = model_instrument.r
        self.Z = model_instrument.Z
        self.F = model_instrument.F
        self.timestep = timestep
        self.dis_r = self.compute_dis_r(loss)

    def compute_qh(self,q_block,f_block):
        """Compute qh at the current timestep using the trapezoidal rule
        
        Arguments :
            q_block : array of q values in the non-zero convolution range
            f_block : array of f values in the non-zero convolution range
        """
        integrand = self.dis_r[0]*(q_block + self.Z*f_block)
        qh = np.sum(integrand)
        return qh
        
        
    def simulate(self, pcontrol, kcontrol, s_time, plot = False, anim_dir=None, delay_anim = 0.001):
        """Simulate the behavior of the instrument for a duration s_timeplt.ylim([-0.1,1.5])

        Args:
            pcontrol (function): mouth pressure as a function of time
            s_time (float): duration of the simulation
            q0 (float): valeur de q à t=0
            f0 (float): valeur de f à t=0
            plot (bool): True to plot simulated q and f as a function of time
            gif_dir (string): if a directory is given, saves an animation of the dynamic behavior at the given directory
        """
            
        N_step = int(s_time // self.timestep) #number of steps to compute
        steps = np.arange(self.dis_r[2], self.dis_r[2]+N_step)
        
        q = np.zeros(N_step + self.dis_r[2])
        f = np.zeros(N_step + self.dis_r[2])
        
        last_state=1 # We consider that  if there is an indertermination at t=0, the bow would stick for the string instruments
        
        for step in steps:
            current_time = (step-self.dis_r[2]) * self.timestep #time associated with the sample being calculated with 0 as the beginning of the simulation
            
            q_block = q[step-self.dis_r[2] : step-self.dis_r[1]+1] #pass only necessary values to compute_qh
            f_block = f[step-self.dis_r[2] : step-self.dis_r[1]+1]
            qh = self.compute_qh(q_block,f_block)
            [q[step], f[step],last_state] = self.intersect(qh, pcontrol(current_time), kcontrol, last_state, q[step-1]) 
                
        qi = 1/2*(q+self.Z*f) #ingoing wave
        qo = 1/2*(q-self.Z*f) #outgoing wave
        fi = 1/self.Z*qi
        fo = -1/self.Z*qi
        
        if anim_dir is not None:
            self.generate_mp4(q, f, pcontrol, kcontrol, anim_dir, delay_anim)
            
        if plot:
            fig = self.plot_sim(q,f,qi,qo,fi,fo)
            return q,f,qi,qo,fi,fo,fig
            
        return q,f,qi,qo,fi,fo
        
    def compute_dis_r(self, loss):
        """Return an array containing a discretized version of 
        the function r[0] between values in r[1] and r[2]
        """
        n_delay = int(self.r[1] // self.timestep) #delay in sample of the first value to compute
        if n_delay==0:
            n_delay=1
        n_end = int(self.r[2] // self.timestep) +1 #delay in sample of the last value to compute
        times = np.linspace(n_delay * self.timestep , n_end * self.timestep, int(n_end-n_delay+1)) #values of time delay to compute f(q)
        dis_r = self.r[0](times) #computed values of r
        energy = np.abs(np.sum(dis_r)*self.timestep)
        dis_r = dis_r / (energy) *self.timestep *(1-loss) #Normalize so that it sums to one
        return np.array([np.flip(dis_r), n_delay, n_end])
    
    def test_reflection(self, time):
        """Plots a simulation of the response to a Heaviside excitation in time, not using the non-linear 
        characteristic. If r is correctly normed, p should go to 0 and f to 2 after an initial transient. 

        Args:
            time (float): Timelength of the simulation
        """
        N_step = int(time // self.timestep)
        steps = np.arange(self.dis_r[2], self.dis_r[2]+N_step)
        qi = np.concatenate((np.zeros(self.dis_r[2]),np.ones(N_step)))
        fi = 1/(self.Z)*qi
        qo = np.zeros_like(qi)
        fo = np.zeros_like(fi)
        
        for step in steps:
            q_block = qi[step-self.dis_r[2] : step-self.dis_r[1]+1] #pass only necessary values to compute_qh
            qo[step] = np.sum(self.dis_r[0]*q_block)
            fo[step] = -1/self.Z*qo[step]
        q = qi+qo
        f = fi+fo
        time = np.arange(len(qi))*self.timestep
        plt.figure()
        plt.title("Réponse à un gradient de pression sans oscillation de l''anche")
        plt.subplot(2,1,1)
        plt.plot(time, q)
        plt.title('Pression')
        plt.ylim([-0.1,1.5])
        plt.subplot(2,1,2)
        plt.plot(time, f)
        plt.title('Débit')
        plt.ylim([-0.1,2.5])
        
    def plot_sim(self,q,f,qi,qo,fi,fo):
        """Plots q, f, qi, qo, fi and fo as functions of time.

        Args:
            q (array): values of q at each timestep
            f (array): values of f at each timestep
            qi (array): values of qi at each timestep
            qo (array): values of qo at each timestep
            fi (array): values of fi at each timestep
            fo (array): values of fo at each timestep

        Returns:
            fig : handle to the figure created
        """
        data = np.array([q,f,qi,qo,fi,fo])
        legends = ["q",
                   "f",
                   "qi",
                   "qo",
                   "fi",
                   "fo"]
        #Creation of an array containing time values at each step
        time_array = np.linspace(0, (len(q)-1)*self.timestep, len(q))
        fig = plt.figure()
        fig, axs = plt.subplots(nrows=6, sharex=True, subplot_kw=dict(frameon=False)) # frameon=False removes frames
        plt.subplots_adjust(hspace=.3)
        for i,ax in enumerate(axs):
            ax.grid()
            ax.plot(time_array, data[i], label=legends[i])
            ax.locator_params(axis="y", nbins=3)
            ax.legend()
        plt.xlabel("Time (s)")
        fig.suptitle("Computed q and f values as functions of time")
        return fig
        
    def generate_mp4(self, q, f, pcontrol, kcontrol, filename, delay_anim):
        """This function generate and saves an mp4 animation representing the dynamic 
        behavior of the system as a function of time as the position of the couple 
        q,f plotted on the non linear function F.

        Args:
            q (array): q at each timestep
            f (array): f at each timestep
            pcontrol (function): value of the control parameter as a function of time
            filename (string): directory and name of the file where to save the figure
        """
        if delay_anim>len(q)*self.timestep:
            print("Error : delay_anim can't be more than simulation duration")
        frame_duration = 30
        x = np.linspace(-1.1*pcontrol(0),1.1*pcontrol(0),500)
        y1 = np.zeros_like(x)
        for i,xi in enumerate(x):
            y1[i]=self.F(xi,pcontrol(0), kcontrol)
        maxy = np.max(y1)
        miny = np.min(y1)
        fig = plt.figure()
        plt.grid('on')
        plt.xlabel('q')
        plt.ylabel('F(q)')
        plt.ylim((miny,maxy))
        plt.xlim((-1.1*pcontrol(0),1.1*pcontrol(0)))
        h1, = plt.plot(x, y1)
        h2 = plt.scatter(0,0,c='r',s=150, marker ='x')
        frames  = np.arange(len(q)-self.dis_r[2]-int(delay_anim/self.timestep)-1)
        def animate(i):
            time = i*self.timestep
            for j,xj in enumerate(x):
                y1[j]=self.F(xj,pcontrol(time+delay_anim), kcontrol)
            h1.set_ydata(y1)
            h2.set_offsets((q[i+self.dis_r[2]+int(delay_anim/self.timestep)],f[i+self.dis_r[2]+int(delay_anim/self.timestep)]))
            return h1, h2
        anim = animation.FuncAnimation(fig, animate, frames = frames, interval=frame_duration, blit=True, repeat=False)
        plt.close(anim._fig)
        anim.save(filename, fps=1/(frame_duration*10**(-3)))


class AutoOscSimulator2ref (AutoOscSimulator1ref):
    """This class implement the general algorithm  to simulate auto-oscillating instruments with 2 reflections in time domain 
    presented by Mcintyre, Woodhouse and Schumacher in 1983
    
    Equations to solve at each time step are the following, where f is the volume flow or the force and q is the pressure or the speed : 
    f = F(q),
    q(t) = q_iL(t)+q_iR + Zf(t) and
    q_iL(t) = rL(t) * (q_iR(t) + Zf(t))    
    q_iR(t) = rR(t) * (q_iL(t) + Zf(t)) 
    
    
    Properties :
        f : non-linear function q(t)-q_h(t)-Zf(t), must be equal to zero at each step.
            takes three parameters : q(t), q_h() and the control parameter p(t).
            If the function accepts more than one solution for a given value of p_h(t),
            it takes a fourth argument giving the last state in order to solve the hysteresis rule.
            
        r : reflection function allowing to compute this ingoing wave from the outgoing wave. Of the form [r,min_value,max_value] where f is the function, 
            and [min_value,max_value] is the interval in which it is not negligible and for which the integral is computed. 
            for wind insruments, r must be expressed with p and q as arguments where p is the pressure inside the mouthpiece and q is the pressure at the entrance of the bore
            
        Z : caracteristic impedance (or admittance) of the medium.
        
        timestep : période d'échantillonage
        
    """
    def __init__(self, model_instrument, timestep, plot_dir=False,loss=0):
        super().__init__(model_instrument, timestep, plot_dir)

    def compute_dis_r(self, loss):
        """Return an array containing a discretized version of 
        the function r[0] between values in r[1] and r[2]
        """
        out = []
        for i in range(2): #We compute both reflection functions
            if self.r[i][1]==self.r[i][2]:
                n_delay = int(self.r[i][1] // self.timestep) 
                n_end = n_delay
                dis_r = -np.array((1))
                out.append(np.array([np.flip(dis_r), n_delay, n_end]))
                continue
            n_delay = int(self.r[i][1] // self.timestep) #delay in sample of the first value to compute
            if n_delay==0:
                n_delay=1
            n_end = int(self.r[i][2] // self.timestep) +1 #delay in sample of the last value to compute
            times = np.linspace(n_delay * self.timestep , n_end * self.timestep, int(n_end-n_delay+1)) #values of time delay to compute f(q)
            dis_r = self.r[i][0](times) #computed values of r
            energy = np.abs(np.sum(dis_r)*self.timestep)
            dis_r = dis_r / (energy) *self.timestep * (1-loss)#Normalize so that it sums to one
            out.append(np.array([np.flip(dis_r), n_delay, n_end]))
        return np.array(out)
    
    def compute_qh(self, qil_block, qir_block, fl_block, fr_block):
        integrandl = self.dis_r[0][0]*(qir_block + self.Z*fl_block)
        qil = np.sum(integrandl)
        integrandr = self.dis_r[1][0]*(qil_block + self.Z*fr_block)
        qir = np.sum(integrandr)
        return qil, qir, qil+qir
    
    def simulate(self, pcontrol, kcontrol, s_time, plot=False, anim_dir=None, delay_anim=0.001):
        """Simulate the behavior of the instrument for a duration s_timeplt.ylim([-0.1,1.5])

        Args:
            pcontrol (function): mouth pressure as a function of time
            s_time (float): duration of the simulation
            q0 (float): valeur de q à t=0
            f0 (float): valeur de f à t=0
            plot (bool): True to plot simulated q and f as a function of time
            gif_dir (string): if a directory is given, saves an animation of the dynamic behavior at the given directory
        """
        max_delay = np.max(self.dis_r[:,2])
        N_step = int(s_time // self.timestep) #number of steps to compute
        steps = np.arange(max_delay, max_delay+N_step)
        
        q = np.zeros(N_step + max_delay)
        qil = np.zeros(N_step + max_delay)
        qir = np.zeros(N_step + max_delay)
        f = np.zeros(N_step + max_delay)
        
        last_state=1 # We consider that  if there is an indertermination at t=0, the bow would stick for the string instruments
        
        for step in steps:
            current_time = (step-max_delay) * self.timestep #time associated with the sample being calculated with 0 as the beginning of the simulation
            
            qil_block = qil[step-self.dis_r[1,2] : step-self.dis_r[1,1]+1] #pass only necessary values to compute integral
            qir_block = qir[step-self.dis_r[0,2] : step-self.dis_r[0,1]+1] #pass only necessary values to compute integral
            fl_block = f[step-self.dis_r[0,2] : step-self.dis_r[0,1]+1]
            fr_block = f[step-self.dis_r[1,2] : step-self.dis_r[1,1]+1]
            qil[step], qir[step], qh = self.compute_qh(qil_block, qir_block, fl_block, fr_block)
            [q[step], f[step],last_state] = self.intersect(qh, pcontrol(current_time), kcontrol, last_state, q[step-1]) 
                
        if anim_dir is not None:
            self.generate_mp4(q, f, pcontrol, anim_dir, delay_anim)
            
        if plot:
            fig = self.plot_sim(q,f)
            return q,f,fig
            
        return q,f
    
    def plot_sim(self, q, f):
        """Plots q, f as functions of time.

        Args:
            q (array): values of q at each timestep
            f (array): values of f at each timestep

        Returns:
            fig : handle to the figure created
        """
        data = np.array([q,f])
        legends = ["q",
                   "f"]
        #Creation of an array containing time values at each step
        time_array = np.linspace(0, (len(q)-1)*self.timestep, len(q))
        fig = plt.figure()
        fig, axs = plt.subplots(nrows=2, sharex=True, subplot_kw=dict(frameon=False)) # frameon=False removes frames
        plt.subplots_adjust(hspace=.3)
        for i,ax in enumerate(axs):
            ax.grid()
            ax.plot(time_array, data[i], label=legends[i])
            ax.locator_params(axis="y", nbins=3)
            ax.legend()
        plt.xlabel("Time (s)")
        fig.suptitle("Computed q and f values as functions of time")
        return fig
  
    
class AutoOscSimulatorDirac(AutoOscSimulator1ref):
    """Same principle as AutoOscSimulator but with a dirac as a reflection function

    Args:
        AutoOscSimulator ([type]): [description]

    Returns:
        [type]: [description]
    """
    def __init__(self, model_instrument, timestep, plot_dir=False, loss = 0):
        self.intersect = model_instrument.intersect
        self.r = model_instrument.r
        self.Z = model_instrument.Z
        self.F = model_instrument.F
        self.timestep = timestep
        self.round_trip = model_instrument.round_trip
        self.dis_r = [np.array([-1])*(1-loss), int(self.round_trip/self.timestep), int(self.round_trip/self.timestep)]
        
    def compute_qh(self,q_block,f_block):
        """Compute qh at the current timestep using the trapezoidal rule
        
        Arguments :
            q_block : array of q values in the non-zero convolution range
            f_block : array of f values in the non-zero convolution range
        """
        return self.dis_r[0]*(q_block + self.Z*f_block)
    
    def simulate(self, pcontrol, k_control, s_time, plot = False, anim_dir=None, delay_anim = 0.001):
        """Simulate the behavior of the instrument for a duration s_timeplt.ylim([-0.1,1.5])

        Args:
            pcontrol (function): mouth pressure as a function of time
            s_time (float): duration of the simulation
            q0 (float): valeur de q à t=0
            f0 (float): valeur de f à t=0
            plot (bool): True to plot simulated q and f as a function of time
            gif_dir (string): if a directory is given, saves an animation of the dynamic behavior at the given directory
        """
            
        N_step = int(s_time // self.timestep) #number of steps to compute
        steps = np.arange(self.dis_r[2], self.dis_r[2]+N_step)
        
        q = np.zeros(N_step + self.dis_r[2])
        f = np.zeros(N_step + self.dis_r[2])
        
        last_state=1 # We consider that  if there is an indertermination at t=0, the bow would stick for the string instruments
        
        for step in steps:
            current_time = (step-self.dis_r[2]) * self.timestep #time associated with the sample being calculated with 0 as the beginning of the simulation
            
            q_block = q[step-self.dis_r[2]] #pass only necessary values to compute_qh
            f_block = f[step-self.dis_r[2]]
            qh = self.compute_qh(q_block,f_block)
            [q[step], f[step],last_state] = self.intersect(qh, pcontrol(current_time), k_control, last_state, q[step-1]) 
                
        qi = 1/2*(q+self.Z*f) #ingoing wave
        qo = 1/2*(q-self.Z*f) #outgoing wave
        fi = 1/self.Z*qi
        fo = -1/self.Z*qi
        
        if anim_dir is not None:
            self.generate_mp4(q, f, pcontrol, anim_dir, delay_anim)
            
        if plot:
            fig = self.plot_sim(q,f,qi,qo,fi,fo)
            return q,f,qi,qo,fi,fo,fig
            
        return q,f,qi,qo,fi,fo

            
    