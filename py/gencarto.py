"""carto generator"""

from utils import call_summary

import subprocess
from os.path import join

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as c
from matplotlib.offsetbox import AnchoredText
from scipy.linalg import hankel
# from smt.sampling_methods import LHS
from sklearn import svm
from numpy.linalg import pinv
from numpy.linalg import eig
from scipy.linalg import svd
from sklearn import metrics
from scipy.stats import qmc

SIMULATOR = "./protopam"


def p2fq(p, dp=69, df=440):
    """convert a MIDI pitch to a frequency in Hz
    defaults are such that the A6 is at 440Hz
    - p: pitch
    - dp: default pitch
    - df: default frequency
    Returns: f frequency of the pitch p in Hz"""
    return 2 ** ((p - dp)/12) * df


def call(mod, **kwargs):
    """call a Faust executable mod with arguments in kwargs
    Precondition: the Faust executable must have one outlet only
    Returns: the list of the simulated samples"""
    cmd = mod + " " + " ".join(f"-{k} {v}" for k, v in kwargs.items())
    return [float(x) for x in subprocess.getoutput(cmd).split("\n")[1:]]


def ESPRIT(x, n, K):
    """ESPRIT Algorithm
    Params
      n: dimension of signal space
      K: number of frequencies"""

    N = len(x)
    el = N-n+1

    # Calcul de la matrice de corrélation
    X = hankel(x[0:n], x[n-1:N])
    X_H = X.conj().T
    Rxx = (1/el)*X@X_H
    [U1, Lambda, U2] = svd(Rxx)

    # Determination de la matrice W
    W = U1[0:n, 0:K]

    # Calcul de W_bas et W_haut
    W_bas = W[0:n-1, 0:K]
    W_haut = W[1:n, 0:K]

    # Calcul de phi, la matrice spectrale
    # Calcul valeurs propres de Phi
    Phi = pinv(W_bas)@W_haut
    z_k, v = eig(Phi)
    delta_k = np.log(abs(z_k))
    f_k = (1/(2*np.pi))*abs(np.angle(z_k))

    return delta_k, f_k


@call_summary
def pitch_gamma(p, s_time=1, n_samples=1000, n_gr=8,
                n=128, K=10, Fe=44100, s_criterion=0.02, f_criterion=5):
    """compute labels depending on parameters
    Params:
      - p: pitch of the fundamental of the instrument
      - s_time: simulation time (in s)
      - n_samples: number of sampled points in control space
      - n_gr: number of sampled points in gr
      - n, K : ESPRIT args
      - s_criterion: sound threshold
      - f_criterion: pitch accuracy threshold
      - Fe: sampling frequency (in Hz)

    Returns:
      - coords, (n_samples x 2) matrix sampled from (γ, ζ) space
      - grpos, n_gr matrix containing all regular values of Gr (ξ)
      - labels, (n_samples x n_gr) matrix such that
        labels[i, j] contains the label of the point
        (coords[i][0], coords[i][1], grpos[j])
    """

    ffreq = p2fq(p)

    sampler = qmc.LatinHypercube(d=2)
    coords = qmc.scale(sampler.random(n=n_samples), [0, 0], [1, 2])
    grpos = np.linspace(0, 1, num=n_gr)
    labels = np.zeros((n_samples, n_gr))

    # regular sampling on Gr (ξ)
    for i, (gamma, zeta) in enumerate(coords):
        for j, xi in enumerate(grpos):
            ns = i*n_gr + j
            if not(ns % 100):
                print("sim %d / %d" % (ns, n_samples * n_gr))
            p = np.array(call(SIMULATOR, gamma=gamma, zeta=zeta, xi=xi,
                              r=Fe, n=Fe*s_time, freq=ffreq))
            N_last = int(2/3*len(p))
            last_sum = 1/N_last*np.sum(p[N_last:]**2)

            if s_criterion < last_sum:

                DELTA, F = ESPRIT(p, n, K)
                F_min = np.min(F[F > 0]) * Fe

                n_cents = 1200*np.log2(F_min / ffreq)

                if np.abs(n_cents) < f_criterion:
                    labels[i, j] = 1
                elif np.abs(n_cents) < 100:
                    labels[i, j] = 2
                else:
                    labels[i, j] = 3
            else:
                labels[i, j] = 0

    return coords, grpos, labels


cMap = c.ListedColormap(['lightgray', 'blue', 'cyan', 'gray'])


def dfname(sim, gri):
    return f"{sim.args['p']}-{gri}"


def svm_de_tranches(sim, dest, fname=dfname, gamma=0.2, C=100):
    """generate maps of regimes using SVM
    - sim: a paminette simulation result wrapped by call_summary
    - dest: the destination directory
    - fname: a function computing a name from a simulation and gr index
    - gamma, C: SVM parameters
    """

    coords, grpos, labels = sim.data

    n_samples = coords.shape[0]
    for j, sl in enumerate(grpos):
        sep = int(.75 * n_samples)  # separates between test and training
        liste_train = 10 * coords[:sep]
        liste_test = 10 * coords[sep:]
        target_train = 10 * labels[:sep, j]
        target_test = 10 * labels[sep:, j]

        clf = svm.SVC(kernel='rbf', gamma=gamma, C=C)
        try:
            clf.fit(liste_train, target_train)
        except ValueError:
            print("problem on gr" + str(j))
            continue
        y_pred = clf.predict(liste_test)
        print("Accuracy:",
              metrics.accuracy_score(target_test, y_pred))
        h = 0.02
        # Plot the decision boundary. we will assign a color to each
        # point in the mesh [x_min, x_max]x[y_min, y_max].
        x_min, x_max = liste_train[:, 0].min() - 1, liste_train[:, 0].max() + 1
        y_min, y_max = liste_train[:, 1].min() - 1, liste_train[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))
        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])

        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        plt.pcolormesh(xx, yy, Z, cmap=cMap)
        # Plot also the training points
        # plt.scatter(liste_train[:, 0], liste_train[:, 1], c=target_train,
        #             cmap=plt.cm.Paired, edgecolors="k")

        # plt.title("gr: %.5f" % sl)
        plt.xlabel('Gamma')
        plt.ylabel('Zeta')
        plt.axis("off")
        plt.gca().add_artist(AnchoredText("gr: %.5f" % sl, loc='upper left'))
        plt.savefig(join(dest, fname(sim, j)),
                    bbox_inches='tight', pad_inches=0)
        # plt.show()
        plt.clf()
        plt.close("all")
