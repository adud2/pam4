"""some utilities to work with"""

import inspect
from functools import wraps
from collections import namedtuple

SimRes = namedtuple('SimRes', ['name', 'doc', 'args', 'data'])


def call_summary(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        sig = inspect.signature(f)
        bargs = sig.bind(*args, **kwargs)
        bargs.apply_defaults()
        return SimRes(f.__name__,
                      f.__doc__,
                      bargs.arguments,
                      f(*args, **kwargs))
    return wrapper
