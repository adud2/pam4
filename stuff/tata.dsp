import("stdfaust.lib");

f(x) = x : +(1);

truc = 1, 2, 3 : *(1), *(2), *(3);
trac = f(1), f(2), f(3);

process = si.dot(3, truc, trac);
