import("stdfaust.lib");

ss = hslider("ssi", 0, 0, 1, 0.25);
sl = hslider("sli", .5, -1, 1, 0.25);
gate = button("gate");

process = select2(gate, os.osc(440), (sl + ss) * os.osc(220) / sl) * .9;
