# Paminette

Mathilde Abrassart, Antonin Dudermel, David Genova, Thomas Risse

## Projet PAM auto-oscillations 2022

Structure:

    .
    ├── ./faust                               programmes faust
    │   └── ./faust/attic                     non utilisé dans le patch final
    ├── ./pd                                  patchs PureData
    │   └── ./pd/maps                         images des cartographies
    ├── ./pics                                quelques images pour rapport
    ├── ./py                                  programmes python
    │   └── ./py/proto                        prototype du simulateur
    │       └── ./py/proto/PAM reflection     simulateur par réflexion
    ├── ./sims                                résultats de simulation
    └── ./stuff                               recherches informelles


## Exemples

Des exemples de simulations et de cartographies sont disponibles en ligne à
l'adresse https://dudermel2.ignorelist.com/s/KkcXfJyPC2nrBGE (l'accès au
serveur peut être un peu lent). Le fichier `maps.zip` est à décompresser dans
le dossier `./pd` pour avoir un patch fonctionnel.

Les résultats des simulations sont stockés sous forme de `pickle` python. Si
vous désirer les ouvrir, il faut que le répertoire de travail de votre shell
python soit `./py`, afin de pouvoir charger le module `utils.py`.

## Compilation

Si les binaires fournis ne fonctionnent pas sur votre machine, il peut être
nécessaire de les recompiler

### Patchs PureData

Les patchs PureData peuvent être compilés en ligne

#### Linux / MacOS

Vous pouvez essayer le Makefile présent (commandes `make`), ce dernier va
tester la présence des outils Faust sur l'ordinateur et basculer sur l'outil en
ligne en cas d'échec. Si PureData crashe avec comme message `illegal hardware
instruction`, alors l'installation de Faust sera nécessaire.

#### Windows

Pas de Makefile présent, vous devez compiler à la main ou en utilisant le
service en ligne.

### Compilation locale

La compilation des patchs se fait avec l'utilitaire `faust2puredata` pour Linux
et MacOS, `faust2w64puredata` ou `faust2w32puredata` pour Windows.

### Compilation en ligne

Si vous ne voulez pas installer Faust, vous pouvez utiliser le service en ligne
https://faustide.grame.fr

Assurez-vous de téléverser l'intégralité des fichiers .dsp et .lib, puis
compilez en utilisant le bouton avec un camion dans le panneau de gauche.
Sélectionnez votre plateforme (linux, osx, windows32) puis l'architecture
puredata.  Dans le fichier `binary.zip` téléchargé se trouve le fichier
`paminette~.pd_linux` (ou `paminette~.pd_darwin` ou `paminette~.dll`) contenant
le patch.

Répétez l'opération avec `filter.dsp`

### Installation

Si `make install` ne fonctionne pas, vous pouvez placer à la main les patchs
PureData dans le dossier `pd` présent à la racine.

### Prototype et Python

Le prototype utilisé pour générer les cartographies est aussi écrit en Faust et
utilise `faust2csvplot`. Malheureusement ce dernier n'est pas disponible en
ligne, la compilation locale est donc nécessaire pour regénérer les
cartographies.

Le fichier `protopam` généré est à placer à l'intérieur du dossier `py` (encore
une fois `make install` devrait suffire).

La version de python conseillée est `3.8` Les utilisateurs de Pipenv peuvent
configurer un environnement à partir du fichier `Pipfile` à la racine. Il est
aussi possible de s'y référer pour avoir la liste des fichiers nécessaires à
l'installation (sous `[packages]`, sauf éventuellement `tk` et `pyqt5`, si
votre matplotlib est bien configuré).

Ceci fait, vous pouvez utiliser le script python `cartos.py`. Notez que pour
générer les cartographies à partir des résultats des simulations, le prototype
n'est pas nécessaire.

Pour plus d'informations, essayez `cartos.py --help`

### Bugs non-résolus

* Ajout d'un échantillonage adaptatif pour avoir des cartographies plus
  précises et moins coûteuses

* Quelques variables globales à éliminer, notamment la première et dernière
  note cartographiée, le nombre de tranches de Gr, dans le subpatch
  `image_carto` du patch `synthese_finale`.

* État indéterminé tant qu'une note n'a pas été jouée
