import("stdfaust.lib");

cl = library("clarinetode.lib");

FCUT = 5000;
N = 20;
    
fq = hslider("fq", 55, 20, 1000, 1); // play frequency
rstG = button("reset"); // reset gate

process = cl.clareed(N, cl.fq2l(fq), rstG, FCUT);
