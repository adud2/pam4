import("stdfaust.lib");
my = library("filter.dsp");

BW = 0.12;
FC = 1800;
K = 4;

k1=-1 * cos(2 * ma.PI * FC/ma.SR);
k2=(1-tan(BW/2))/(1+tan(BW/2));

a0 = 1;
a1 = k1 * (1+k2);
a2 = k2;

Q0 = k2;
Q1 = k1 * (1+k2);
Q2 = 1;

b0 = (a0 * (1+K) + Q0  * (1-K))/2;
b1 = (a1 * (1+K) + Q1 * (1-K))/2;
b2 = (a2 * (1+K)+ Q2 * (1-K))/2;

dw=hslider("drywet",0,0,1,0.001);

process = _ <:(fi.iir((b0,b1,b2),(a1,a2)) * 0.5 : *(1-dw)),*(dw):+;
