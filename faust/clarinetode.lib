declare name "Clarinet ODE simulator library";
declare version "0.0.1";

import("stdfaust.lib");
import("consts.lib");

// modes index start at 1

// frequency to length converter (clip to 1 Hz)
fq2l(f) = C / (4 * fp)
with{
  fp = max(f, 1);
};

l2fq(len) = C / (4 * len);

// angular frequency of the n-th mode
angFreq(n, len) = (2 * n - 1) * ma.PI * C / 2 / len;
dAngFreq(n, len) = angFreq(n, len) / ma.SR;

// damping ratio
dampR(n, len) = ABS / IRAD * sqrt(VISC * len / DENS / C / gn)
		+ 1/2 * gn * (IRAD / len)^2
with{
  gn = (2 * n - 1) * ma.PI / 2;
};

// odd extension of the square root to negative numbers
oSqrt(x) = sqrt(abs(x)) * ma.signum(x);

// computes the flow given pressure control
//  - diffp : pressure difference between player mouth and instrument
//  - zeta : mouth parameter

flow(diffp, zeta) =
  select2(diffp > 1, zeta * (1-diffp) * oSqrt(diffp), 0);

dif = _ <: (_ - _') / ma.SR; // discrete differentiator

// a reseter to purge NaN/infs from memory
rst(r) = select2(r, _, 0);

// p(i, len) pressure of ith mode given length len
// in: dif(flow) (discrete derivative of the flow)
// out: p_i pressure of ith mode

p(i, len, r) = ( + , *(flC) : + ) ~ (rst(r) <: *(pf), *(ppf)')
with{
  // discrete damping coeff (in 1 / samples)
  dDampC = 2 * dAngFreq(i, len) * dampR(i, len);
  pf = (2 - dDampC - dAngFreq(i, len)^2);
  ppf = -1 + dDampC;
  flC = 2 * C / len;
};

// lmin(i, fcut) minimal length such that the frequency of the ith mode is
// smaller than a cut frequency
//   - i : number of the mode
//   - fcut : the cut frequency
//out:
//   - length

lmin(i, fcut) = fq2l(fcut) * (2 * i - 1);

pcond(i, len, fcut) =  len > lmin(i, fcut);

ppar(n, len, r, fcut) =
  _ <: par(i, n, pcond(i + 1, len, fcut),
	   p(i+1, max(lmin(i + 1, fcut), len), r)) :
  par(i, n, select2(_, 0, _)) :> _;

nm = ro.cross(2) : - ;

// the model
// in:
//  - n : number of modes
//  - len : length of the tube
//  - r : reset gate
//  - fcut : cut frequency
//  - _ : control gamma (mouth pressure)
//  - _ : control zeta
// out:
//  - pressure in the mouthpiece
//  - flow in the mouthpiece

clar(n, len, r, fcut) =
  ((ppar(n, len, r, fcut) <: _, _), _, _ : _, (nm , _ : flow))
  ~ (!,_ : rst(r): dif);

// =================== REED PART =======================

// linear model for the reed movement
// in
//  - diffp: pressure difference between player mouth and instrument
//  - zeta: zeta control parameter
//  - gr: reed damping control (by player lips)

lreed(r, diffp, zeta, gr) = ( + , inC : + ) ~ (rst(r) <: *(pf), *(ppf)')
with{
  // discretized gr parameter
  dGr = gr / ma.SR;
  pf = (2 - dGr - DAF_REED^2);
  ppf = dGr - 1;
  inC = DAF_REED^2 * (diffp + zeta);
};

// introduces non-linearities in the reed
// in
//   - rp reed position (in linear model)
// out
//   - reed position, non-linear

nlreed(rp) = select2(rp > 0.6, rp, 1 - C_nlr * exp(-Knlr * rp));

// computes the flow given pressure control and reed position
// in
//  - rp : reed position
//  - diffp : pressure difference between player mouth and instrument
// out
//  - flow

flowreed(diffp, rp) = select2(rp >= 1, Ucoeff * (1 - rp) * oSqrt(diffp), 0);

// compute the flow given only diffp and control parameters
// in
//  - _ : pressure difference between player mouth and instrument
//  - _ : zeta control
//  - _ : gr control (not normalized)

flowRctrl(r) = (_ <: _, _), _, _ : _, (lreed(r) : nlreed) : flowreed;

// the model with reed
// in:
//  - n : number of modes
//  - len : length of the tube
//  - r : reseter
//  - fc : frequency
//  - _ : control gamma (mouth pressure)
//  - _ : control zeta
//  - _ : control gr (unnormalized
// out:
//  - pressure in the mouthpiece
//  - flow in the mouthpiece

clareedu(n, len, r, fc) = 
  (((ppar(n, len, r, fc) <: _, _), _ : _, nm), _, _ : _, flowRctrl(r))
  ~ (!,_ : rst(r): dif);
// same as above but normalized

clareed(n, len, r, fc) = _, _, (*(2000): +(3000)): clareedu(n, len, r, fc);
