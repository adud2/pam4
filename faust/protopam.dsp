import("stdfaust.lib");

cl = library("clarinetode.lib");

FCUT = 5000;
N = 20;
    
freq = hslider("freq", 55, 20, 1000, 1); // play frequency
rstG = button("reset"); // reset gate

gm = hslider("gamma", 0, 0, 1, 0.0009765625);
ze = hslider("zeta", 0, 0, 2, 0.0009765625);
gr = hslider("xi", 0, 0, 1, 0.0009765625);

process = cl.clareed(N, cl.fq2l(freq), rstG, FCUT, gm, ze, gr): _,!;
