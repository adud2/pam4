import("stdfaust.lib");

qc = -1;
Z = 1;

C = 340;
L = 1;


F(q, pcontrol, k) = select2(q < qc, k * (pcontrol - q) * (q - qc), 0);
 

intersect(qh, pcontrol, k) =
  select2(qh < qc,
          (-b + sqrt(delta)) / 2 / a,
          qh)
with {
  a = 1;
  b = 1 / Z / k - (pcontrol + qc);
  c = -qh / Z / k + (pcontrol * qc);
  delta = b ^ 2 - 4 * a * c;
};

qh = _, ! : -1 * @ (int(L/C * ma.SR));

pass = qh, (_, _ <: _, _, _, _) : (intersect <: _,_ ), _, _ : _, F;

clarinet = pass ~ (_,_);

pcontrol = hslider("pcontrol", 1.7, 0, 4, 0.0625);
k = hslider("k", 0.4, 0, 2, 0.0625);
gain = hslider("gain", 1, 0, 1, 0.0625);

//ph(n) = _ ~ (+(1/n) : fmod(_, 1));

process = pcontrol, k : clarinet : *(gain), ! : min(0.999) : max(-.999);
